<?php
/**
 * 
 * checking in users Admin side
 *
 * @author 		AJDE
 * @category 	Admin
 * @package 	eventon-qr/classes
 * @version     0.1
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class evoqr_admin{
	
	public $optRS;
	function __construct(){
		add_filter('eventon_settings_tab1_arr_content', array( $this, 'qr_settings' ) ,10,1 );
		add_filter('eventon_settings_lang_tab_content', array( $this, 'language' ), 10, 1);

	}

	// QR code settings into eventon settings
		function qr_settings($array){
			$pages = new WP_Query(array('post_type'=>'page'));
			$_page_ar[]	='--';
			while($pages->have_posts()	){ $pages->the_post();								
				$page_id = get_the_ID();
				$_page_ar[$page_id] = get_the_title($page_id);
			}
			wp_reset_postdata();

			// get all available templates for the theme
				$templates = get_page_templates();
				$_templates_ar['archive-ajde_events.php'] = 'Default Eventon Template';
				$_templates_ar['page.php'] = 'Default Page Template';
			   	foreach ( $templates as $template_name => $template_filename ) {
			       $_templates_ar[$template_filename] = $template_name;
			   	}

			$new_array= $array;
			$new_array[]= array(
				'id'=>'eventon_qr',
				'name'=>'Settings for QR Code checking',
				'display'=>'none',
				'tab_name'=>'QR Code',
				'top'=>'4',
				'fields'=> apply_filters('evo_se_setting_fields', array(
					
					array('id'=>'evoqr_001','type'=>'checkboxes','name'=>'Select user roles that is allowed to checkin guests. (Default is administrator)',
						'options'=>array(
							'editor'=>'Editor',
							'author'=>'Author',
							'contributor'=>'Contributor',
							'subscriber'=>'subscriber',
					)),
					)
				));
			
			return $new_array;

		}

	// language
		function language($_existen){
			$new_ar = array(
				array('type'=>'togheader','name'=>'ADDON: QR Codes'),
				
				
				array('label'=>'Checkin Page','type'=>'subheader'),
					array('label'=>'Successfully un-checked ticket!','name'=>'evoQR_001',),
					array('label'=>'Ticket already un-checked!','name'=>'evoQR_002',),
					array('label'=>'Successfully Checked!','name'=>'evoQR_003',),
					array('label'=>'Already checked!','name'=>'evoQR_004',),
					array('label'=>'Un-check this ticket','name'=>'evoQR_005',),
					array('label'=>'Missing or invalid Ticket ID','name'=>'evoQR_006',),
					array('label'=>'You do not have permission!','name'=>'evoQR_007',),
				array('type'=>'togend'),
				array('type'=>'togend'),
			);
			return (is_array($_existen))? array_merge($_existen, $new_ar): $_existen;
		}
}
new evoqr_admin();
