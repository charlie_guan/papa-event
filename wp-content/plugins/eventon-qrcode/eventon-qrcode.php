<?php
/*
 Plugin Name: EventON - QR Code
 Plugin URI: http://www.myeventon.com/
 Description: Checkin customers with QR Code
 Author: Ashan Jay
 Version: 0.2
 Author URI: http://www.ashanjay.com/
 Requires at least: 3.7
 Tested up to: 4.0

 */


class EventON_qr{
	
	public $version='0.2';
	public $eventon_version = '2.2.22';
	public $name = 'QR Code';
			
	public $addon_data = array();
	public $slug, $plugin_slug , $plugin_url , $plugin_path ;
	private $urls;
	public $template_url ;
	
	// Construct
	public function __construct(){
		$this->super_init();

		include_once( 'admin/class-admin_check.php' );
		$this->check = new addon_check($this->addon_data);
		$check = $this->check->initial_check();
		
		if($check){
			$this->addon = new evo_addon($this->addon_data);
			add_action('plugins_loaded', array($this, 'plugin_init'));
				
		}			
	}

	// check for secondary required addons
		function plugin_init(){
			if(class_exists('evotx') || class_exists('EventON_rsvp')){
				add_action( 'init', array( $this, 'init' ), 0 );
				$this->includes();		
			}else{
				add_action('admin_notices', array($this, '_secondary_eventon_warning'));
			}
		}
	
	// SUPER init
		function super_init(){
			// PLUGIN SLUGS			
			$this->addon_data['plugin_url'] = path_join(WP_PLUGIN_URL, basename(dirname(__FILE__)));
			$this->addon_data['plugin_slug'] = plugin_basename(__FILE__);
			list ($t1, $t2) = explode('/', $this->addon_data['plugin_slug'] );
	        $this->addon_data['slug'] = $t1;
	        $this->addon_data['plugin_path'] = dirname( __FILE__ );
	        $this->addon_data['evo_version'] = $this->eventon_version;
	        $this->addon_data['version'] = $this->version;
	        $this->addon_data['name'] = $this->name;

	        $this->plugin_url = $this->addon_data['plugin_url'];
	        $this->plugin_slug = $this->addon_data['plugin_slug'];
	        $this->slug = $this->addon_data['slug'];
	        $this->plugin_path = $this->addon_data['plugin_path'];
		}

	// INITIATE please
		function init(){				
			// Activation
			$this->activate();		
			
			// Deactivation
			register_deactivation_hook( __FILE__, array($this,'deactivate'));

			// RUN addon updater only in dedicated pages
			if ( is_admin() ){
				$this->addon->updater();			
			}

			//frontend includes
			if ( ! is_admin() || defined('DOING_AJAX') ){
				//include_once( 'includes/class-frontend.php' );
			}

			$this->checkin = new evoqr_checkin();			
		}
	

	/** Include required core files. */
		function includes(){
			include_once( 'class-checkin.php' );			
			if ( is_admin() ){
				include_once( 'admin/admin-init.php' );
			}
		}


	// ACTIVATION			
		function activate(){
			// add actionUser addon to eventon addons list
			$this->addon->activate();
		}		
	
		// Deactivate addon
		function deactivate(){
			$this->addon->remove_addon();
		}
		function _secondary_eventon_warning(){
	        ?>
	        <div class="message error"><p><?php _e('Eventon QR Code require either RSVP or EventTickets addon to function properly. Please install/activate either one of those.', 'eventon'); ?></p></div>
	        <?php
	    }

}

// Initiate this addon within the plugin
$GLOBALS['eventon_qr'] = new EventON_qr();	