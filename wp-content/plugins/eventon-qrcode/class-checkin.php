<?php
/**
 * 
 * checking in users
 *
 * @author 		AJDE
 * @category 	Admin
 * @package 	eventon-qr/classes
 * @version     0.2
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
class evoqr_checkin{
	
	public $optQR;
	public $opt2;
	function __construct(){
		$this->optQR = get_option('evcal_options_evcal_1');
		$this->opt2 = get_option('evcal_options_evcal_2');

		add_action( 'init', array( $this, 'init' ) ,15);	
		add_shortcode('add_eventon_checkin', array($this,'checkinShortcode'));
		add_filter( 'template_include', array( $this, 'template_loader' ) , 99);

		// add QR code to eventon addons
		//rsvp addon
			add_action('eventonrs_rsvp_post_table', array($this, 'show_qr_code'), 10, 2);
			add_action('eventonrs_confirmation_email', array($this, 'show_qr_code_2'), 10, 2);
		// event ticekts addon
			add_filter('evotx_tixPost_tixid', array($this, 'show_qr_code_TX'), 10, 1);
			add_filter('evotx_email_tixid_list', array($this, 'show_qr_code_TX2'), 10, 1);
	}

	// initiate things
		public function init(){
			global $wpdb, $post, $eventon_qr;

			// create checking page if doesnt exists
			$_checking_page = get_option('_eventon_checking_page');
			$checkin_page = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name='checkin'");

			if(empty($checkin_page) && (empty($_checking_page) || $_checking_page!= 1)	){
				$path = AJDE_EVCAL_PATH;
				require_once( $path.'/includes/admin/eventon-admin-install.php' );

				$content = '[add_eventon_checkin]';

				eventon_create_page( esc_sql( _x( 'checkin', 'page_slug', 'eventon' ) ), 'eventon_checkin_page_id', __( 'Checkin', 'eventon' ), $content );

				update_option('_checking_page',1);
			}

			// styles
			wp_register_style('evo_checkin',$eventon_qr->plugin_url.'/assets/checkin_styles.css' );

		}

	// output checking page content
		public function checkinShortcode($atts){
			ob_start();
				echo 'EventOn';
			return ob_get_clean();
		}

	// template loading
		public function template_loader( $template ) {
			global $eventon_qr, $post, $eventon;
			
			$file='';
			
			// Paths to check
			$paths = apply_filters('eventon_template_paths', array(
				0=>TEMPLATEPATH.'/',
				1=>TEMPLATEPATH.'/'.$eventon->template_url.'checkin/', // eg. E:\xampp\htdocs\WP/wp-content/themes/twentythirteen/eventon/checkin/
			));
			
			$checkin_page_id = get_option('eventon_checkin_page_id');

			// check if this page is checkin page
			if( !empty($post) && !empty($checkin_page_id) &&  $post->ID == $checkin_page_id ) {
				$file 	= 'checking.php';					
				$paths[] 	= $eventon_qr->addon_data['plugin_path']  . '/templates/';

				// add special class to body				
				add_filter('body_class',array($this,'browser_body_class'));

				// styles for this page
				wp_enqueue_style( 'evo_checkin');
			}
			

			// FILE Exist
			if ( $file ) {				
				// each path
				foreach($paths as $path){				
					if(file_exists($path.$file) ){	
						$template = $path.$file;	
						break;
					}
				}
					
				if ( ! $template ) { 
					$template = $eventon_qr->addon_data['plugin_path'] . '/templates/' . $file;				
				}
			}
			

			//echo $checkin_page_id;
			//print_r($template);
			
			return $template;
		}

		function browser_body_class($classes=''){			
			$classes[] = 'evocheckin';
			return $classes;
		}

	// QR Code for rsvp
		public function get_qr_code($tix_id, $repeat_interval='', $size=120){
			$siteurl = get_bloginfo('url');
			$siteurl = $siteurl.'/checkin/?id='.$tix_id. ( !empty($repeat_interval)? '&ri='.$repeat_interval: null);
			$chl = urlencode($siteurl);

			return '<img src="https://chart.googleapis.com/chart?chs='.$size.'x'.$size.'&cht=qr&chl='.$chl.'"/>';
		}

		// show QR code for addons
			// RSVP addon
			public function show_qr_code($rsvpid, $rsvp_pmv){
				$repeat_interval = (!empty($rsvp_pmv['repeat_interval']))? $rsvp_pmv['repeat_interval'][0]:0;
				echo "<tr><td>QR Code: </td><td>".$this->get_qr_code($rsvpid, $repeat_interval)."</td></tr>";
			}
			public function show_qr_code_2($rsvpid, $rsvp_pmv){
				$repeat_interval = (!empty($rsvp_pmv['repeat_interval']))? $rsvp_pmv['repeat_interval'][0]:0;				
				?>
					<p style="color:#303030; text-transform:uppercase; font-size:18px; font-style:italic; padding-bottom:0px; margin-bottom:0px; line-height:110%;">QR Code</p>
					<p style="color:#afafaf; font-style:italic;font-size:14px; margin:0 0 10px 0; padding-bottom:10px;">You can use the below QRcode to checkin at the event</p>
					<p><?php echo $this->get_qr_code($rsvpid, $repeat_interval);?></p>
				<?php
			}
			//Ticket addon
				public function show_qr_code_TX($code){
					return "<em style='float:left; padding-right:10px'>".$this->get_qr_code($code,'',90)."</em><em style='padding-top:25px; display:inline-block'>". $code."</em>";
				}
				public function show_qr_code_TX2($code){
					return "<em style='float:left; padding-right:10px'>".$this->get_qr_code($code,'',90)."</em><em style='padding:34px 0; display:inline-block; line-height:18px;'>". $code."</em>";
				}


	// actual checking page data
		function checkin_page_content(){

			$tixid = (!empty($_GET['id'])? $_GET['id']: null);

			// differentiate ID type
			if(strpos($tixid, '-')){
				$tt = explode('-', $tixid);
				$id_type = get_post_type($tt[0]);
			}else{
				$id_type = get_post_type($tixid	);
			}

			$_after ='';

			ob_start();
			
			// check if have permission to checkin tickets
			$_permission_grants = false;

			if(is_user_logged_in()){
				global $current_user, $wp_roles;
				$allowed_roles = array('administrator');

				// add other user roles set via settings for checking in guests
				if(!empty($this->optQR['evoqr_001'])){
					$allowed_roles = array_merge($allowed_roles, $this->optQR['evoqr_001']);
				}
			
				foreach($allowed_roles as $role){
					if(array_key_exists($role, $current_user->caps)){
						$_permission_grants = true;
					}
				}
			}

			if($_permission_grants){
				if(!empty($tixid) && ($id_type=='evo-rsvp' || $id_type=='evo-tix' )){

					// Tickets
					if($id_type=='evo-tix'){

						$ticket_item_id = explode('-', $tixid);
						$ticketItem = new evotx_TicketItem($ticket_item_id[0]);
						
						// modified snippet #20-08-2015#
						// added exceptions handling #28-08-2015#
						$post_id=$ticket_item_id[0];
						$order_id=$ticket_item_id[1];
						
						try{
							$mysqli = new mysqli("127.0.0.1", "ticketAdmin", "qc/h8Z~4y@lkf2t", "ticket", 3306);
							if ($mysqli->connect_errno) {
								throw new Exception( "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error );
							}
						}
						catch(Exception $e) {
							echo $e->getMessage();
						}
						
						
						try{
							$sql = "SELECT * FROM `wp_postmeta` WHERE post_id=$post_id AND meta_key IN ('qty', 'cost')";
							if( !($result = $mysqli->query($sql)) ) {
								throw new Exception( "Ticket ID $post_id does not exist in the database");
							}
						}
						catch(Exception $e) {
							echo $e->getMessage();
						}
						
						try{
							if( $result->num_rows < 1){
								throw new Exception( "Ticket ID $post_id does not exist in the database");
							}
							while ($row = $result->fetch_assoc()) {
							
								if( $row['meta_key'] == "qty"){
									$qty = $row['meta_value'];
								} elseif( $row['meta_key'] == 'cost'){
									$total_cost = $row['meta_value'];
								}
							}
							$unit_price = $total_cost / $qty;
						}
						catch(Exception $e) {
							echo $e->getMessage();
						}
						
						try{
							$sql = "SELECT * FROM `wp_postmeta` WHERE post_id=$order_id AND meta_key IN ('_shipping_first_name', '_shipping_last_name', '_billing_phone')";
							if( !($result = $mysqli->query($sql)) ) {
								throw new Exception( "Order ID $order_id does not exist in the database");
							}
						}
						catch(Exception $e) {
							echo $e->getMessage();
						}
						
						try{
							if( $result->num_rows < 1){
								throw new Exception( "Order ID $order_id does not exist in the database");
							}
							while ($row = $result->fetch_assoc()) {
							
								if( $row['meta_key'] == '_shipping_first_name'){
									$firstName = $row['meta_value'];
								} elseif( $row['meta_key'] == '_shipping_last_name'){
									$lastName = $row['meta_value'];
								} elseif( $row['meta_key'] == '_billing_phone'){
									$phoneNumber = $row['meta_value'];
								}
							}
						}
						catch(Exception $e) {
							echo $e->getMessage();
						}
						
						try{
							$mysqli->query("SET character_set_results=utf8");
							$sql = "SELECT * FROM `wp_woocommerce_order_items` WHERE order_item_type='line_item' AND order_id=$order_id";
							if(!($result = $mysqli->query($sql) )){
								throw new Exception( "Order ID $order_id does not exist in the database");
							}
						}
						catch(Exception $e) {
							echo $e->getMessage();
						}
						
						try{
							if( $result->num_rows < 1){
								throw new Exception( "Order ID $order_id does not exist in the database");
							}
							
							$row = $result->fetch_assoc();
							$event_name = $row['order_item_name'];
							if( $pos = strpos($event_name, ')') ){
								$event_name = substr($event_name, ($pos+1));
							}
							
						}
						catch(Exception $e) {
							echo $e->getMessage();
						}
						
						$mysqli->close();
						// end modified snippet

						$current_status = $ticketItem->get_ticket_status($tixid);

						// uncheck ticket
						if(!empty($_GET['action']) && $_GET['action']=='unc'){
							// change status to check-in
							if(!empty($current_status) && $current_status =='checked'){
								
								$ticketItem->change_ticket_status('check-in', $tixid, $ticket_item_id[0]);

								$_yes_no = 'yes';
								$_msg = eventon_get_custom_language($this->opt2, 'evoQR_001', 'Successfully un-checked ticket!');
								//echo '01';
							}else{
								$_yes_no = 'almost';
								$_msg = eventon_get_custom_language($this->opt2, 'evoQR_002', 'Ticket already un-checked!');
								//echo '02';
							}
						}else{
							//check in a ticket 					
							if(empty($current_status) || $current_status =='check-in'){
								$ticketItem->change_ticket_status('checked', $tixid, $ticket_item_id[0]);

								$_yes_no = 'yes';
								$_msg = eventon_get_custom_language($this->opt2, 'evoQR_003', 'Successfully Checked!');
								//echo '03'.$current_status;
							}else{// already checkedin status = not-empty|checked
								$_yes_no = 'almost';
								$_msg = eventon_get_custom_language($this->opt2, 'evoQR_004', 'Already checked!');
								$_after = "<p class='mart20'><a class='btn' href='?id={$tixid}&action=unc'>".eventon_get_custom_language($this->opt2, 'evoQR_005', 'Un-check this ticket')."</a></p>";		
								//echo '04';			
							}
						}


					}else{ // RSVP 
						$tix_status = get_post_meta($tixid, 'status', true);
						//echo $tix_status;

						// uncheck a checked ticket
						if(!empty($_GET['action']) && $_GET['action']=='unc'){

							// change status to check-in
							if(!empty($tix_status) && $tix_status =='checked'){
								update_post_meta($tixid, 'status','check-in');
								$_yes_no = 'yes';
								$_msg = eventon_get_custom_language($this->opt2, 'evoQR_001', 'Successfully un-checked ticket!');
							}else{
								$_yes_no = 'almost';
								$_msg = eventon_get_custom_language($this->opt2, 'evoQR_002', 'Ticket already un-checked!');
							}
						}else{

							//check in a ticket 					
							if(empty($tix_status) || $tix_status =='check-in'){
								update_post_meta($tixid, 'status','checked');
								$_yes_no = 'yes';
								$_msg = eventon_get_custom_language($this->opt2, 'evoQR_003', 'Successfully Checked!');
							}else{// already checkedin status = not-empty|checked
								$_yes_no = 'almost';
								$_msg = eventon_get_custom_language($this->opt2, 'evoQR_004', 'Already checked!');
								$_after = "<p class='mart20'><a class='btn' href='?id={$tixid}&action=unc'>".eventon_get_custom_language($this->opt2, 'evoQR_005', 'Un-check this ticket')."</a></p>";					
							}
						}
					}

				}else{ // missing ticket id
					$_yes_no = 'no';
					$_msg = eventon_get_custom_language($this->opt2, 'evoQR_006', 'Missing or invalid Ticket ID');
				}
			}else{
				$_yes_no = 'no';
				$_msg = eventon_get_custom_language($this->opt2, 'evoQR_007', 'You do not have permission!');
			}	
			?>
	
				<div class='evo_checkin_page <?php echo $_yes_no;?>'>
					<h2>Ticket #</h2>
					<h3><?php echo $tixid;?></h3>

					<p class='sign <?php echo $_yes_no;?>'></p>
					<h4><?php echo $_msg;?></h4>
					<?php 
						if(isset($event_name)){
							echo "<h6>Event: $event_name</h6>";
						}
						if(isset($unit_price)){
							echo "<h6>Price: $$unit_price</h6>";
						}
						
						if((isset($firstName)) || (isset($lastName))){
							echo "</br><h6>Name: $firstName $lastName</h6>";
						}
						if(isset($phoneNumber)){
							echo "<h6>Phone: $phoneNumber</h6>";
						}
						
						echo $_after;?>
			</div>
		<?php
			echo ob_get_clean();


		}
}
