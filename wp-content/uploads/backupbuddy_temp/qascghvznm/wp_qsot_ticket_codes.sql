CREATE TABLE `wp_qsot_ticket_codes` (  `order_item_id` bigint(20) unsigned NOT NULL,  `ticket_code` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,  PRIMARY KEY (`order_item_id`),  KEY `tc` (`ticket_code`(191))) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wp_qsot_ticket_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_qsot_ticket_codes` ENABLE KEYS */;
