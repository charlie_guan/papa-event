CREATE TABLE `wp_banner_stats` (  `stat_id` bigint(20) NOT NULL AUTO_INCREMENT,  `banner_id` bigint(20) NOT NULL,  `date` date NOT NULL,  `clicks` bigint(20) NOT NULL,  `views` bigint(20) NOT NULL,  PRIMARY KEY (`stat_id`),  KEY `banner_id` (`banner_id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40000 ALTER TABLE `wp_banner_stats` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_banner_stats` ENABLE KEYS */;
